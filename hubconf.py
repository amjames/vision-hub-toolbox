dependencies = ['torchvision']

def _create_classifier_binding(name):
    """Generate a function which we will assign directly into globals to make it a callable at module scope"""
    # Imports at function scope to not pollute the namespace
    import torchvision
    from jatic_toolbox.interop.torchvision import TorchVisionClassifier
    import sys

    def trim_docstring(docstring):
        if not docstring:
            return ''
        # Convert tabs to spaces (following the normal Python rules)
        # and split into a list of lines:
        lines = docstring.expandtabs().splitlines()
        # Determine minimum indentation (first line doesn't count):
        indent = sys.maxsize
        for line in lines[1:]:
            stripped = line.lstrip()
            if stripped:
                indent = min(indent, len(line) - len(stripped))
        # Remove indentation (first line is special):
        trimmed = [lines[0].strip()]
        if indent < sys.maxsize:
            for line in lines[1:]:
                trimmed.append(line[indent:].rstrip())
        # Strip off trailing and leading blank lines:
        while trimmed and not trimmed[-1]:
            trimmed.pop()
        while trimmed and not trimmed[0]:
            trimmed.pop(0)
        # Return a single string:
        return '\n'.join(trimmed)



    def truncate_model_docs(func):
        """scrape out the stuff from the vision docstring before the params, this is where the model info, refs etc will be"""
        docstr = getattr(func, '__doc__', "")
        partition = docstr.find('Args:')
        return docstr[:partition]

    def add_generic_model_docs(base_docs, return_typ):
        add = f"""
              Parameters
              ----------
              weights : Optional[str]
                  The weights to load into the model
                  defaults to 'DEFAULT'
                  if `None`, no weights are loaded (randomly initialized state)
                  if `str`, the weights with the given name are loaded (see below)
              with_processor : bool
                  Use the weights transforms to set the processor slot , default True
                  an error will be raised if `with_processor=True` is combined with `weights=None`
              **config :  Any
                  Additional kwargs forwarded to model builder, see torchvision docs.

              Return
              ------
              model : {return_typ.__name__} 
              """
        return base_docs + "\n" + trim_docstring(add)

    def add_weights_note(base_docs, weights_enum):
        add = """
              .. note::
                 The following weights are registered for this model:
                 """
        indent_space = len(add.split()[-1])
        weights_lines = ("\n" + (indent_space * " ")).join([k for k in weights_enum.__members__.keys()])
        return base_docs + '\n' + trim_docstring(add + weights_lines)



    def generate_wrapped_classifier_ep(name):
        builder = torchvision.models._api.get_model_builder(name)
        weights_enum = torchvision.models._api._get_enum_from_fn(builder)
        doc_string = truncate_model_docs(builder)
        doc_string = add_generic_model_docs(doc_string, TorchVisionClassifier)
        doc_string = add_weights_note(doc_string, weights_enum)

        def ep(*, weights='DEFAULT', with_processor=True, **config) -> TorchVisionClassifier:
            model = builder(**config)
            if weights is not None:
                weights = weights_enum[weights]
            labels = None if weights is None else weights.meta['categories']
            processor = None
            if with_processor:
                processor = weights.transforms()
            return TorchVisionClassifier(model, processor, labels)
        ep.__doc__  = doc_string
        ep.__name__ = name
        return ep
    
    return generate_wrapped_classifier_ep(name)


def _get_model_names():
    import torchvision
    # according to the vision docs this collects all classifier models
    return torchvision.models.list_models(module=torchvision.models)


# register model entrypoints
for name in _get_model_names():
    globals()[name] = _create_classifier_binding(name)

# cleanup 
del _create_classifier_binding
del _get_model_names

# we could do something in the same sprit for datasets but those are not as discoverable so it might require some additional work 